<?php
/**
 * Adding a simple mask as helper, can be used in combination with Sub-question validation equation
 *
 * @author Denis Chenu <denis@sondages.pro>
 * @copyright 2019-2022 Denis Chenu <http://www.sondages.pro>
 * @license AGPL v3
 * @version 0.2.0
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU AFFERO GENERAL PUBLIC LICENSE as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
class jquerySimpleMask extends PluginBase
{
    static protected $name = 'jquerySimpleMask';
    static protected $description = 'Adding a simple mask as helper on text question.';

    protected $settings = array(
        'information' => array(
            'type' => 'info',
            'content' => '<p>See <a href="http://igorescobar.github.io/jQuery-Mask-Plugin/" rel="external">jQuery-Mask-Plugin</a> for example and demo.</p>'
                .'',
        ),
    );

    public function init() {
        /* question part */
        $this->subscribe('beforeQuestionRender','setSimpleMaskOnInput');
        $this->subscribe('newQuestionAttributes','addSimpleMaskAttributes');
    }

    /**
     * Add the option in each question
     */
    public function addSimpleMaskAttributes()
    {
        if (!$this->getEvent()) {
            throw new CHttpException(403);
        }
        $questionAttributes['jquerySimpleMask_mask']=array(
            "types"=>'SQ;',
            'category'=>$this->_translate('Mask'),
            'sortorder'=>10,
            'inputtype'=>'text',
            'default'=>"",
            "help"=>sprintf($this->_translate('See %s for example and demo.'),'<a href="http://igorescobar.github.io/jQuery-Mask-Plugin/" rel="external">jQuery-Mask-Plugin</a>'),
            "caption"=>$this->_translate('Mask for this input.'),
        );
        $questionAttributes['jquerySimpleMask_reverse']=array(
            "types"=>'SQ;',
            'category'=>$this->_translate('Mask'),
            'sortorder'=>20,
            'inputtype'=>'switch',
            'options'=>array(
                0=>gT('No'),
                1=>gT('Yes')
            ),
            'default'=>1,
            "help"=>$this->_translate('Filling by end of mask, else by start.'),
            "caption"=>$this->_translate('Reverse'),
        );
        $questionAttributes['jquerySimpleMask_clearIfNotMatch']=array(
            "types"=>'SQ;',
            'category'=>$this->_translate('Mask'),
            'inputtype'=>'switch',
            'sortorder'=>30,
            'options'=>array(
                0=>gT('No'),
                1=>gT('Yes')
            ),
            'default'=>0,
            "help"=>$this->_translate('Filling by start , default is by end.'),
            "caption"=>$this->_translate('Clear if not match'),
        );
        $questionAttributes['jquerySimpleMask_placeholder']=array(
            "types"=>'SQ;',
            'category'=>$this->_translate('Mask'),
            'sortorder'=>40,
            'inputtype'=>'text',
            'default'=>"",
            "help"=>$this->_translate('Add a placeholder to the input.'),
            "caption"=>$this->_translate('Place holder'),
        );
        $this->getEvent()->append('questionAttributes', $questionAttributes);
    }

    /**
     * Add the script and activate mask on question render
     */
    public function setSimpleMaskOnInput()
    {
        if (!$this->getEvent()) {
            throw new CHttpException(403);
        }
        $qid = $this->getEvent()->get('qid');
        $aAttributes = \QuestionAttribute::model()->getQuestionAttributes($qid);
        if(!empty($aAttributes['jquerySimpleMask_mask']) ) {
            $mask = CHtml::encode($aAttributes['jquerySimpleMask_mask']);
            $options = array(
                'reverse' => intval($aAttributes['jquerySimpleMask_reverse']),
                'clearIfNotMatch' => $aAttributes['jquerySimpleMask_clearIfNotMatch'],
                'placeholder' => $aAttributes['jquerySimpleMask_placeholder'],
            );
            $script = "$('#question{$qid} .answer-item.text-item input:text').mask('{$mask}',".json_encode($options).");\n";
            /* @todo : check with ajax */
            $this->_registerPackage();
            App()->clientScript->registerScript("jquerySimpleMask{$qid}",$script,CClientScript::POS_END);
        }
    }

    private function _registerPackage()
    {
        Yii::setPathOfAlias(get_class($this),dirname(__FILE__));
        $min = (App()->getConfig('debug') > 1 ) ? '.min' : '';

        if(!Yii::app()->clientScript->hasPackage('jQuery-Mask-Plugin')) { // Not tested with 3 and older devbridge-autocomplete
            Yii::app()->clientScript->addPackage('jQuery-Mask-Plugin', array(
                'basePath'    => get_class($this).'.assets.jQuery-Mask-Plugin',
                'js'          => array('jquery.mask'.$min.'.js'),
                'depends'      =>array('jquery'),
            ));
        }
        /* @todo add own translation ? 'w': {pattern: /\d/}, 'c': {pattern: /[a-z]/}, , 'C': {pattern: /[A-Z]/}, … ? */
        /* Registering the package */
        Yii::app()->getClientScript()->registerPackage('jQuery-Mask-Plugin');
    }

    /** Common helpers **/

    /**
     * Translate a string
     * @param string to translate
     * @param string escape mode
     * @parm string language to use
     * @return string
     */
    private function _translate($string, $sEscapeMode = 'unescaped', $sLanguage = NULL) {
        if(intval(Yii::app()->getConfig('versionnumber')) >= 3) {
            return parent::gT($string, $sEscapeMode, $sLanguage );
        }
        return $string;
    }
}
